import './Modal.css'
import order from './../public/order.png'
import React, { PureComponent } from 'react'


class Modal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
                  sorStatus: 0,
                  sortClass: 'ModalSort',
                  datas: props.datas,
                  showModal: props.showModal,
                  handleClose: props.handleClose,
                  sorterLogic: props.sorterLogic
                };

  }

  determinateusableShow = () => {
    return 'ModalBlock ' + (this.state.showModal ? 'display-block' : 'display-none');
  }

  determinateSortShow = (newStatus) => {
    return newStatus === 1 ? 'ModalSort inUse'
            : newStatus === -1 ? 'ModalSort revert'
            : 'ModalSort';
  }

  manageSorter = () => {
    const datasNew = this.state.sorterLogic(this.state.sorStatus);
    const newStatus = (this.state.sorStatus === 0 ? 1 : (this.state.sorStatus * -1));
    this.setState({ ...this.state, datas: datasNew, sorStatus: newStatus, sortClass: this.determinateSortShow(newStatus)});
  }

    render() {
      this.setState({ ...this.state, datas: this.props.datas, showModal: this.props.showModal });
      if (this.state.datas && this.state.datas.length > 0) {
        return (
                  <div className={this.determinateusableShow()}>
                    <div className="ModalCore">
                      <div className="ModalText">
                      <ul>
                        {this.state.datas.map(player =>
                          <div>{player.name + '; goles: ' + player.goles}</div>
                        )}
                      </ul>
                      </div>
                      <div className="ModalClose">
                        <button onClick={this.state.handleClose}>
                          Cerrar
                        </button>
                        <img onClick={this.manageSorter} className={this.state.sortClass} src={order}/>
                      </div>
                    </div>
                  </div>
                );
      } else {
        return '';
      }
    }
}
export default Modal
