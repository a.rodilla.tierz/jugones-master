import reactSvg from './react.svg'
import './App.css'

import React, { PureComponent } from 'react'
const domain = 'http://localhost:3001'

import Modal from './Modal'

class App extends PureComponent {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
              players: [],
              teams: [],
              classManagerHeader: 'headerArea',
              showModal: false,
              pichichis: [],
            };
  }

  componentWillMount() {
    fetch(`${domain}/teams`)
      .then(response => {
        return response.json();
      })
      .then(teams => {
        this.setState({ ...this.state, teams: teams, classManagerHeader: 'headerArea', showModal: false });
      });

      window.addEventListener('scroll', (event) => {
        if (event.pageY >= this.myRef.current.clientHeight && this.state.classManagerHeader != 'headerArea sticky') {
          this.setState({ ...this.state, classManagerHeader: 'headerArea sticky' });
        } else if (event.pageY < this.myRef.current.clientHeight && this.state.classManagerHeader != 'headerArea') {
          this.setState({ ...this.state, classManagerHeader: 'headerArea' });
        }
      });
  }

  componentDidMount() {
    fetch(`${domain}/players`)
      .then(response => {
        return response.json();
      })
      .then(players => {
        this.setState({ ...this.state, players: players });
      });
  }

determinatePichichis = () => {
  fetch(`${domain}/pichichis`)
    .then(response => {
      return response.json();
    })
    .then(pichichis => {
      const usablePichichis = pichichis.reduce((acc, playerPichi) => {
        const payerData = this.state.players.find((playerNode) => {return playerNode.idPlayer === playerPichi.playerId;});
        acc.push({
          idPlayer: playerPichi.playerId,
          goles: this.determinateGoals(playerPichi.goals),
          name: payerData.name
        });
        return acc;
      }, []);
      this.setState({ ...this.state, showModal: !this.state.showModal, pichichis: usablePichichis });
    });
}

  determinateGoals = (goals) => {
    if (!goals) {
      return 0;
    } else if(typeof goals === "string") {
      const listdata = goals.split(' ');
      return parseInt((listdata[0] || listdata[1]), 10);
    } else {
      return goals;
    }

  }

  hideModal = () => {
    if (!this.state.showModal) {
      this.determinatePichichis();
    } else {
      this.setState({ ...this.state, showModal: !this.state.showModal });
    }
  }

  sorterLogic = (sortIndex) => {
    const pichichisSorted = sortIndex === 0 ? this.state.pichichis.sort((a, b) => {
                                                  return a.goles > b.goles;
                                                })
                            : this.state.pichichis.reverse();
    this.setState({ ...this.state, pichichis: pichichisSorted });
    return pichichisSorted;
  }

  render() {
    return <div className="App">
    <Modal showModal={this.state.showModal} handleClose={this.hideModal} sorterLogic={this.sorterLogic} datas={this.state.pichichis} />
      <header ref={this.myRef} className="App-heading App-flex">
        <h2>Bienvenido a la prueba de los equipos</h2>
      </header>
      <div className="App-teams App-flex">
        {/*
          TODO ejercicio 2
          Debes obtener los players en lugar de los equipos y pintar su nombre.
          Borra todo el código que no sea necesario. Solo debe existir un título: Los jugadores
          y una lista con sus nombres.
          ** Los comentarios de los ejercicios no los borres.
        */}
        <div className={this.state.classManagerHeader}>
          <button onClick={this.hideModal}>
            Pichichis
          </button>
        </div>
        <ul>
          {/*
            TODO ejercicio 3
            Vamos a pasar a darle diseño. Crea el diseño propuesto en el readme con los requerimientos que se necesite.
            Guiate por las imágenes.
           */}
          {this.state.players.map(player => <div className='blockPlayers'>
                                    <img className='imagePlayers' src={player.img} />
                                    <div className='blockTextPlayers'>
                                      <div className='namePlayers'>
                                        {player.name}
                                        <span>
                                          {' ' + player.position}
                                        </span>
                                      </div>
                                      <div className='nameTeam'>
                                        {(this.state.teams.find((team) => { return team.id === player.teamId; })).name}
                                      </div>
                                    </div>
                                    <img className='imgTeam' src={(this.state.teams.find((team) => { return team.id === player.teamId; })).shield} />
                                  </div>)}
        </ul>
      </div>
      <div className="App-instructions App-flex">
        <img className="App-logo" src={reactSvg}/>
        <p>Edit <code>src/App.js</code> and save to hot reload your changes.</p>
      </div>
    </div>
  }
}

export default App
