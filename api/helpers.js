const getPlayer = (playerId, teams) =>  {
  var player
  var team = teams.find(t => {
    player = t.players.find(p => p.id === playerId)
    return player
  })

  if (player && team) {
    return { player, teamId: team.id }
  }

  return false
}

const buildernomealTeam = (teamData, POSITIONS_STRING) => {
  return teamData.players.map((playerDatas) => {
    return {
              name: playerDatas.name,
              idPlayer: playerDatas.id,
              position: POSITIONS_STRING[playerDatas.position],
              img: playerDatas.img || playerDatas.url,
              teamId: teamData.id
            };
  });
}

const builderAtleicTeam = (teamData, POSITIONS_STRING, atleticoImages) => {
  return teamData.players.map((playerDatas) => {
    return {
              name: playerDatas.name,
              idPlayer: playerDatas.id,
              position: POSITIONS_STRING[playerDatas.position],
              img: atleticoImages[playerDatas.id],
              teamId: teamData.id
            };
  });
}

module.exports = {
  getPlayer,
  buildernomealTeam,
  builderAtleicTeam
}
